/*
visit every nodes one time
*/

//#include <GOD>
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>
#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<ll,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
typedef long long ll;
using namespace std;
ll n , m;
bool vis[20];
ll path[20];
vector<ll> vec[20];
bool DFS(ll x , ll num){
    vis[x]=true;
    if(num==n){
        for(auto i : vec[path[num-1]])
            if(i==0)
                return true;
        return false;
    }
    for(auto i : vec[x]){
        path[num]=i;
        if(!vis[i] && DFS(i ,num+1)) return true;
        path[num]=-1;
    }
    vis[x]=false;
    return false;
}
int main(){
 //   Test;
    ll t; cin>>t;
    while(t--){
        Set(vis, false);
        Set(path, -1);
        cin>>n>>m;
        Rep(i,n+1) vec[i].clear();
        Rep(i ,m){ll x ,y ; cin >>x>>y;x-- ;y--; vec[x].push_back(y) ; vec[y].push_back(x);}
        path[0]=0;
        bool ans =DFS(0 ,1);
        if(ans){
        Rep(i ,n) cout<<path[i]+1<<" ";
        cout<<path[0]+1<<endl;
        }
        if(ans) cout<<1<<endl;
        else cout<<0<<endl;
    }
}